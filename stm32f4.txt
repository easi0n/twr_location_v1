Configuration	stm32f4
STM32CubeMX 	4.27.0
Date	06/13/2019
MCU	STM32F401CCUx



PERIPHERALS	MODES	FUNCTIONS	PINS
I2C1	I2C	I2C1_SCL	PB6
I2C1	I2C	I2C1_SDA	PB7
RCC	Crystal/Ceramic Resonator	RCC_OSC_IN	PH0 - OSC_IN
RCC	Crystal/Ceramic Resonator	RCC_OSC_OUT	PH1 - OSC_OUT
SPI1	Full-Duplex Master	SPI1_MISO	PA6
SPI1	Full-Duplex Master	SPI1_MOSI	PA7
SPI1	Full-Duplex Master	SPI1_SCK	PA5
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
USART1	Asynchronous	USART1_RX	PA10
USART1	Asynchronous	USART1_TX	PA9



Pin Nb	PINs	FUNCTIONs	LABELs
5	PH0 - OSC_IN	RCC_OSC_IN	
6	PH1 - OSC_OUT	RCC_OSC_OUT	
10	PA0-WKUP	GPIO_Output	
11	PA1	GPIO_Output	
12	PA2	GPIO_Output	
13	PA3	GPIO_Output	
15	PA5	SPI1_SCK	
16	PA6	SPI1_MISO	
17	PA7	SPI1_MOSI	
18	PB0	GPIO_EXTI0	
30	PA9	USART1_TX	
31	PA10	USART1_RX	
42	PB6	I2C1_SCL	
43	PB7	I2C1_SDA	



SOFTWARE PROJECT

Project Settings : 
Project Name : stm32f4
Project Folder : D:\STM32F401\stm32f4
Toolchain / IDE : MDK-ARM V5
Firmware Package Name and Version : STM32Cube FW_F4 V1.21.0


Code Generation Settings : 
STM32Cube Firmware Library Package : Copy all used libraries into the project folder
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : No
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : 





