#ifndef BPHERO_UWB_H
#define BPHERO_UWB_H

#include "frame_header.h"
#include "common_header.h"

//rx为基站，tx为标签
//#define RX_NODE
#define TX_NODE

//基站节点地址0x0001 0x0002 0x0003
#ifdef RX_NODE
 #define SHORT_ADDR 0x0001
 //#define LCD_ENABLE //没有液晶的时候，把这个宏定义注释掉
#endif  

//标签和基站地址不能重叠
//标签节点地址 0x0005 0x0006 0x0007
#ifdef TX_NODE
 #define SHORT_ADDR 0x0005
 // #define LCD_ENABLE //没有液晶的时候，把这个宏定义注释掉
#endif  



extern int psduLength ;
extern srd_msg_dsss msg_f_send ; // ranging message frame with 16-bit addresses

#ifndef SPEED_OF_LIGHT
#define SPEED_OF_LIGHT      (299702547.0)     // in m/s in air
#endif
/* Buffer to store received frame. See NOTE 1 below. */
#ifndef FRAME_LEN_MAX
	#define FRAME_LEN_MAX 127
#endif

#ifndef TX_ANT_DLY
#define TX_ANT_DLY 0
#endif

#ifndef RX_ANT_DLY
#define RX_ANT_DLY 32900
#endif

#define BP_UWB_INIT 1
#define BP_UWB_BROADCAST 2
#define BP_UWB_PAIR 3
#define BP_UWB_TRAMSMIT 4

#define FINAL_MSG_POLL_TX_TS_IDX 2
#define FINAL_MSG_RESP_RX_TS_IDX 6
#define FINAL_MSG_FINAL_TX_TS_IDX 10
#define FINAL_MSG_TS_LEN 4

/* UWB microsecond (uus) to device time unit (dtu, around 15.65 ps) conversion factor.
 * 1 uus = 512 / 499.2 ? and 1 ? = 499.2 * 128 dtu. */
#define UUS_TO_DWT_TIME 65536

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.46 ms with above configuration. */
#define POLL_RX_TO_RESP_TX_DLY_UUS 2600
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define RESP_TX_TO_FINAL_RX_DLY_UUS 500
/* Receive final timeout. See NOTE 5 below. */
#define FINAL_RX_TIMEOUT_UUS 3300

/* Delay between frames, in UWB microseconds. See NOTE 4 below. */
/* This is the delay from the end of the frame transmission to the enable of the receiver, as programmed for the DW1000's wait for response feature. */
#define POLL_TX_TO_RESP_RX_DLY_UUS 150
/* This is the delay from Frame RX timestamp to TX reply timestamp used for calculating/setting the DW1000's delayed TX function. This includes the
 * frame length of approximately 2.66 ms with above configuration. */
#define RESP_RX_TO_FINAL_TX_DLY_UUS 3000 //2700 will fail
/* Receive response timeout. See NOTE 5 below. */
#define RESP_RX_TIMEOUT_UUS 5700
extern uint8 rx_buffer[FRAME_LEN_MAX];
/* Hold copy of status register state here for reference, so reader can examine it at a breakpoint. */
extern uint32 status_reg;
/* Hold copy of frame length of frame received (if good), so reader can examine it at a breakpoint. */
extern uint16 frame_len ;
extern void BPhero_UWB_Message_Init(void);
extern void BPhero_UWB_Init(void);//dwm1000 init related

#endif
