/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
#include "kalman.h"
static void Handle_TimeStamp(void);

/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/

/* Private functions ---------------------------------------------------------*/
#define MAX_ANTHOR_NODE 40
struct distance_struct
{
    double rx_distance;
    struct time_timestamp tx_node;
    struct time_timestamp rx_node;
    bool present;
    int count;
} bphero_distance[MAX_ANTHOR_NODE];

typedef signed long long int64;
typedef unsigned long long uint64;
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;
static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;

//#define MAX_TAG 20
static srd_msg_dsss *msg_f ;
static int freq_count = 0;
static double tof;

static double distance_temp = 0;
static double distance[256] = {0};

static int sum_distance = 0;
static int distance_count = 0;
extern dwt_config_t config;

void Simple_Rx_Callback()
{
    uint32 status_reg = 0,i=0;
    uint32 poll_tx_ts, resp_rx_ts, final_tx_ts;
    uint32 poll_rx_ts_32, resp_tx_ts_32, final_rx_ts_32;
    double Ra, Rb, Da, Db;
    int64 tof_dtu;
    char dist_str[16] = {0};
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);

    if (status_reg & SYS_STATUS_RXFCG)
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f->sourceAddr[1];
            msg_f_send.seqNum = msg_f->seqNum;
            switch(msg_f->messageData[0])
            {
                case 'P':
                    msg_f_send.messageData[0]='A';//Poll ack message
                    int temp = (int)(distance[msg_f_send.destAddr[0]]*100);//convert m to cm
                    msg_f_send.messageData[1]=temp/100;
                    msg_f_send.messageData[2]=temp%100;
//                    dwt_writetxdata(ANCH_RESPONSE_MSG_LEN, (uint8 *)&msg_f_send, 0) ; // write the frame data
//                    dwt_writetxfctrl(ANCH_RESPONSE_MSG_LEN, 0);
								    dwt_writetxdata(14, (uint8 *)&msg_f_send, 0) ; // write the frame data
                    dwt_writetxfctrl(14, 0);

                    /* Start transmission. */
                    dwt_starttx(DWT_START_TX_IMMEDIATE);
                    //MUST WAIT!!!!!
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };
									  poll_rx_ts = get_rx_timestamp_u64();

                    break;

                case 'F':
                    resp_tx_ts = get_tx_timestamp_u64();
                    final_rx_ts = get_rx_timestamp_u64();
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_POLL_TX_TS_IDX], &poll_tx_ts);
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_RESP_RX_TS_IDX], &resp_rx_ts);
                    final_msg_get_ts(&msg_f->messageData[FINAL_MSG_FINAL_TX_TS_IDX], &final_tx_ts);

                    poll_rx_ts_32 = (uint32)poll_rx_ts;
                    resp_tx_ts_32 = (uint32)resp_tx_ts;
                    final_rx_ts_32 = (uint32)final_rx_ts;
                    Ra = (double)(resp_rx_ts - poll_tx_ts);
                    Rb = (double)(final_rx_ts_32 - resp_tx_ts_32);
                    Da = (double)(final_tx_ts - resp_rx_ts);
                    Db = (double)(resp_tx_ts_32 - poll_rx_ts_32);
                    tof_dtu = (int64)((Ra * Rb - Da * Db) / (Ra + Rb + Da + Db));

                    tof = tof_dtu * DWT_TIME_UNITS;
                    distance_temp = tof * SPEED_OF_LIGHT;
                    distance[msg_f_send.destAddr[0]] = distance_temp - dwt_getrangebias(config.chan,(float)distance_temp, config.prf);//�����ȥ����ϵ��
                    //kalman filter
                    //distance[msg_f_send.destAddr[0]] = KalMan(distance[msg_f_send.destAddr[0]]);
                  //  printf("0x%04X <--> 0x%02X%02X :%.02f cm\n",SHORT_ADDR,msg_f_send.destAddr[1],msg_f_send.destAddr[0],distance[msg_f_send.destAddr[0]]);
//                    sprintf(dist_str, "  Dis: %.02f m", distance[msg_f_send.destAddr[0]]);//m
//                    OLED_ShowString(0, 6,dist_str);
                    break;

                case 'M':
										USART_puts(&msg_f->messageData[1],16);
                    break;

                default:
                    break;
            }
        }
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_setrxtimeout(0);
        dwt_rxenable(0);
    }
    else
    {
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //enable recive again
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_rxenable(0);
    }
}

int rx_main(void)
{
    OLED_ShowString(0,0,"   51UWB Node");
    OLED_ShowString(0,4,"  www.51uwb.cn");
    OLED_ShowString(0,2,"    Rx Node ");

    dwt_setrxtimeout(0);
    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_rxenable(0);
    bphero_setcallbacks(Simple_Rx_Callback);
    KalMan_Init();

    if(SHORT_ADDR == 0x0001)
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
    if(SHORT_ADDR == 0x0002)
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
    if(SHORT_ADDR == 0x0003)
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
    while (1)
    {
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
